\begin{lemma}{Left Spine to \Xbwm~Transform}{}
    Let $L$ be a left spine of height $h \geq 1$, rooted in a tree $T$ possibly equal to $L$ itself. Also let $a$ be the index of the root node of $L$, $b = a + 2h$ the index of the last node of $L$ and $S$ the \Xbwm~transform of $T$. Then we have that

    \begin{align}
	\sla[i] & =
	\begin{cases}
	    0, \quad & a + 1 \leq i \leq a + h \\
	    1, \quad & a + h + 1 \leq i \leq b
	    \label{eq.spine.left.last}
	\end{cases}
	\\
	\spi[i] & =
	\begin{cases}
	    \sal[i - 1] \spi[i - 1], \quad & a + 1 \leq i \leq a + h \\
	    \spi[k - i], \quad & a + h + 1 \leq i \leq b
	    \label{eq.spine.left.pi}
	\end{cases}
    \end{align}

    where $k = 2(a + h) + 1$.
\end{lemma}

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}
	\graph [tree layout] {
	    A/$\alpha_0$ -- [dashed] {
		/$\circ$,
	    },
	    A -- {
		a/$\alpha_a$ -- {
		    a1/$\alpha_{a + 1}$,
		    a2/$\alpha_{a + 2}$
		},
	    },
	    A -- [dashed] {
		/$\circ$,
		/$\circ$
	    },
	};
    \end{tikzpicture}
    \caption{A tree $T$ with a left spine of height $h = 1$.}
    \label{fig.prop.left.to.transform}
\end{figure}

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|c|}
	\hline
	$i$ & $\sla$ & $\sal$ & $S_{\pi}$ \\
	\hline
	0 & 0 & $\alpha_0$ & $\varepsilon$ \\
	1 & ? & ? & $\alpha_0$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$a$ & ? & $\alpha_a$ & $\pi_a$ \\
	$a + 1$ & 0 & $\alpha_{a + 1}$ & $\alpha_a \pi_a$ \\
	$b = a + 2$ & 1 & $\alpha_{a + 2}$ & $\alpha_a \pi_a$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$|T| - 1$ & ? & ? & ? \\
	\hline
    \end{tabular}
    \caption{The $S[a, a + 2]$ portion of the \Xbwm~transform $S$.}
    \label{tab.prop.left.to.transform}
\end{table}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
	\graph [tree layout] {
	    A/$\alpha_0$ -- [dashed] {
		/$\circ$,
	    },
	    A -- {
		a/$\alpha_a$ -- {
		    a1/$\alpha_{a + 1}$ -- [dashed] {
			/$\circ$ -- {
			    a3/$\alpha_{a + h}$,
			    a4/$\alpha_{a + h + 1}$,
			},
			/$\alpha_{b - 1}$
		    },
		    a2/$\alpha_b$
		},
	    },
	    A -- [dashed] {
		/$\circ$,
		/$\circ$
	    },
	};
    \end{tikzpicture}
    \caption{A tree $T$ with a left spine of height $h > 1$.}
    \label{fig.prop.left.to.transform.inductive}
\end{figure}

\begin{proof}
    We proceed by induction on $h$.

    \paragraph{Base Case}
    We have $h = 1$, and the tree $T$ is shown in \figref{fig.prop.left.to.transform}.

    Clearly, nodes of $T$ whose index $i$ is such that $i < a$ or $a + 2 < i$ have no impact on $S[a, a + 2]$, the portion of \Xbwm~transform whose entries are indexed from $a$ to $a + 2$.

    From \tabref{tab.prop.left.to.transform}, where we represent $S[a, a + 2]$, it is obvious that

    \begin{align}
	\sla[a + 1] & = 0 \\
	\sla[a + 2] & = 1 \\
	\spi[a + 1] & = \spi[a + 2] = \sal[a] \spi[a]
    \end{align}

    \paragraph{Inductive Step}
    In this case, $h > 1$, and a left spine of height $h' = h - 1$ is rooted at node $a + 1$ (\figref{fig.prop.left.to.transform.inductive}).

    Therefore, by the inductive hypothesis

    \begin{align}
	\sla[i] & =
	\begin{cases}
	    0, & \quad a + 2 \leq i \leq a + 1 + h' \\
	    1, & \quad a + 2 + h' \leq i \leq b - 1
	\end{cases}
	\\
	\spi[i] & =
	\begin{cases}
	    \sal[i - 1] \spi[i - 1], & \quad a + 2 \leq i \leq a + 1 + h' \\
	    \spi[k - i], & \quad a + 2 + h' \leq i \leq b - 1
	\end{cases}
    \end{align}

    with $k = 2(a + 1 + h') = 2(a + h) + 1$. We finally have to consider nodes $a + 1$ and $b$. It is evident that $\sla[a + 1] = 0$, $\sla[b] = 1$ and $\spi[a + 1] = \spi[b] = \sal[a] \spi[a]$. The inductive hypothesis and this last point lead us to

    \begin{align}
	\sla[i] & =
	\begin{cases}
	    0, & \quad i = a + 1 \lor a + 2 \leq i \leq a + 1 + h' \\
	    1, & \quad i = b \lor a + 2 + h' \leq i \leq a + 1 + 2h'
	\end{cases}
	\\ & =
	\begin{cases}
	    0, & \quad a + 1 \leq i \leq a + h \\
	    1, & \quad a + 1 + h \leq i \leq b
	\end{cases}
	\\
	\spi[i] & =
	\begin{cases}
	    \sal[i - 1] \spi[i - 1], & \quad i = a + 1 \lor a + 2 \leq i \leq a + h \\
	    \spi[k - i], & \quad i = b \lor a + 2 + h \leq i \leq b - 1
	\end{cases}
	\\ & =
	\begin{cases}
	    \sal[i - 1] \spi[i - 1], & \quad a + 1 \leq i \leq a + h \\
	    \spi[k - i], & \quad a + 1 + h \leq i \leq b
	\end{cases}
    \end{align}
\end{proof}
