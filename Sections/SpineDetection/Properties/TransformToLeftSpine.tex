\begin{lemma}{\Xbwm~Transform to Left Spine}{}
    Let $S$ be an \Xbwm~transform of length $n$. Then if $\exists a, b \mid b - a = 2h$, with $h \geq 1$, conditions \eqref{eq.spine.left.last} and \eqref{eq.spine.left.pi} hold and $\forall i \mid a + h \leq i \leq b$

    \begin{equation}
	% TODO Do we really need this to hold for every i? Wouldn't is_leaf(b) and is_leaf(b - 1) be sufficient?
	\isleaf{i}
	\label{eq.spine.left.leaf}
    \end{equation}

    the subtree $U$ encoded by the subportion $S[a, b]$ of $S$ is a left spine of height $h$. \label{lemma.transform.to.left}
\end{lemma}

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|c|}
	\hline
	$i$ & $\sla$ & $\sal$ & $S_{\pi}$ \\
	\hline
	0 & 0 & $\alpha_0$ & $\varepsilon$ \\
	1 & ? & ? & $\alpha_0$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$a$ & ? & $\alpha_a$ & $\pi_a$ \\
	$a + 1$ & 0 & $\alpha_{a + 1}$ & $\alpha_a \pi_a$ \\
	$b = a + 2$ & 1 & $\alpha_{a + 2}$ & $\alpha_a \pi_a$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$n - 1$ & ? & ? & ? \\
	\hline
    \end{tabular}
    \caption{The $S[a, a + 2]$ portion of the \Xbwm~transform $S$.}
    \label{tab.prop.transform.left}
\end{table}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
	\graph [tree layout] {
	    /$\alpha_a$ -- {
		/$\alpha_{a + 1}$,
		/$\alpha_b$
	    }
	};
    \end{tikzpicture}
    \caption{A left spine of height $h = 1$.}
    \label{fig.prop.transform.left}
\end{figure}

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|c|}
	\hline
	$i$ & $\sla$ & $\sal$ & $S_{\pi}$ \\
	\hline
	0 & 0 & $\alpha_0$ & $\varepsilon$ \\
	1 & ? & ? & $\alpha_0$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$a$ & ? & $\alpha_a$ & $\pi_a$ \\
	$a + 1$ & 0 & $\alpha_{a + 1}$ & $\alpha_a \pi_a$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$a + h$ & 0 & ? & $\alpha_{a + h - 1} \pi_{a + h - 1}$ \\
	$a + h + 1$ & 1 & ? & $\alpha_{a + h - 1} \pi_{a + h - 1}$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$b - 1$ & 1 & ? & $\alpha_{a + 1} \pi_{a + 1}$ \\
	$b$ & 1 & ? & $\alpha_a \pi_a$ \\
	$\vdots$ & $\vdots$ & $\vdots$ & $\vdots$ \\
	$n - 1$ & ? & ? & ? \\
	\hline
    \end{tabular}
    \caption{The $S[a, a + 2]$ portion of the \Xbwm~transform $S$.}
    \label{tab.prop.transform.left.inductive}
\end{table}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
	\graph [tree layout] {
	    /$\alpha_a$ -- {
		/$\alpha_{a + 1}$ -- {
		    /$\circ$ -- [dashed] {
			/$\circ$ -- {
			    /$\alpha_{a + h}$,
			    /$\alpha_{a + h + 1}$ 
			},
			/$\circ$
		    },
		    /$\circ$
		},
		/$\alpha_b$
	    }
	};
    \end{tikzpicture}
    \caption{A left spine of height $h = 1$.}
    \label{fig.prop.transform.left.inductive}
\end{figure}

\begin{proof}
    By induction on $h$.

    \paragraph{Base Case}
    If $h = 1$, then we have the \Xbwm~transform from \tabref{tab.prop.transform.left}

    Given a generic node indexed $i$, its parent node has index $j$, where

    \begin{equation}
	j = \max \left\{ j' \mid 0 \leq j' < i \land \pi_i = \alpha_j \pi_j \right\}
    \end{equation}

    Nodes $a + 1$ and $b$ have node $a$ as their parent, since $\pi_{a + 1} = \pi_b = \alpha_a \pi_a$, $a < a + 1 < b$ and $a$ is the greatest index satisfying these conditions. Furthermore, $a + 1$ and $b$ are the only child nodes of $a$ (because $\sla[b] = 1$).

    To complete the base case, consider nodes $i$ such that $i < a$. These nodes can have no influence over the structure of the subtree rooted at $a$. However, it may happen that nodes $i \mid b < i$, are descendants of $b$. This case is ruled out by the fact that $\isleaf{b}$. Therefore the structure of the subtree rooted at $a$ matches that of \figref{fig.prop.transform.left}

    \paragraph{Inductive Step}
    In this case, we have $b - a = 2h$, with $h > 1$, and $S[a, b]$ satisfies conditions \eqref{eq.spine.left.last}, \eqref{eq.spine.left.pi} and \eqref{eq.spine.left.leaf}. Of course, these conditions still hold for $a' = a + 1$, $b' = b - 1$, and since $b' - a' = a - b - 2 = 2(h - 1) = 2h'$, we can apply the inductive hypothesis to \eqref{eq.spine.left.last} and claim that $S[a', b']$ encodes a left spine of height $h'$.

    To conclude the proof, observe nodes $a + 1$ and $b$. By a reasoning analogous to that for the base case, $a + 1$ and $b$ are the only children of node $a$, and so they form the subtree of \figref{fig.prop.transform.left.inductive}

    Since node $a + 1$ is the root of a left spine of height $h'$, $a$ is the root of a left spine of height $h' + 1 = h$.
\end{proof}
