\section{An Algorithm for Spine Detection}

\begin{theorem}{}{}
    Let $T$ be a tree of arbitrary shape, and $S$ its \Xbwm~transform. Then an algorithm exists that is able to detect all left and right spines of $T$ in $\Theta(|T|)$ time and $\Theta(1)$ space.
\end{theorem}

\begin{listing}[h]
    \begin{minted}[escapeinside=||]{C}
	function spines (automaton)
	    spines = |$\emptyset$|

	    while automaton.has_next()
		    automaton.next()

		    if automaton.state == ACCEPTED
			    spines = spines |$\cup$| automaton.spine_range()
			    automaton.reset_state()
	    
	    return spines
    \end{minted}
    \caption{Spine detection algorithm.}
    \label{algo.spine.detection}
\end{listing}

\subimport{DetectionAlgorithm/}{LeftAutomaton}
\subimport{DetectionAlgorithm/}{RightAutomaton}

\begin{proof}
    The proof is constructive, and deploys an algorithm that performs the detection  of all left and right spines of $T$ with a single scan of $S$ in constant space. We give the pseudocode of this algorithm in \algoref{algo.spine.detection}.

    The algorithm receives as input a particular finite-state automaton, and returns \texttt{spines}, the set of all $(a, b)$ pairs such that $a$ and $b$ are the first- and last-node index of a spine in $T$, respectively.
    The finite-state automaton that is fed into the algorithm does not define a language, but maintains the state of the current scan of the vector $S$. It comes in two varieties, a left and a right one.

    This automaton is initialized with the vector $S$ and is stepped forward by the \texttt{next()} operation. The \texttt{has\_next()} operation returns \texttt{true} as long as the automaton hasn't finished iterating on $S$. Whenever the automaton detects a new spine, \texttt{automaton.state} equals the special valute \texttt{ACCEPTED}, and the $(a, b)$ pair of this spine can be obtained by the \texttt{spine\_range()} operation. \algoref{algo.spine.detection} simply manipulates this automaton until it comes to the end of vector $S$. \linebreak

    To explain the left and right automata in more detail, consider \figref{fig.automaton.left} and \figref{fig.automaton.right}. The graphical notation can be interpreted as follows: each node represents a state in the ordinary sense of finite-state automata, but each edge connects two nodes $a$ and $b$ if and only if the conditions enclosed by the parentheses are satisfied when the state is $a$. Sometimes, a horizontal rule separates this list of conditions from a set of instructions, that are executed on the transition from $a$ to $b$. The aggregate of conditions is identified, when necessary, by an $(x)$ at the top-right corner, where $x$ is a number.

    We describe the logic of the left automaton in detail, and only mention that of the right one, which is similar. What the automaton does is to simply follow the pattern described by \lemmaref{lemma.transform.to.left}: it looks for an ``upward'' series of $h$ $\spi$ values, followed by a ``downward'' series of other $h$ values that are ``symmetric'' to the first group. In the meanwhile, it also checks for the correct series of $\sla$ values, that is, a contiguous run of $h$ $0$s followed by a contiguous run of $h$ $1$s. This is done by maintaining some internal variables, like $i$, which is automatically increased at each step, and a few others.

    The automaton starts at the \texttt{reset} state, and may eventually enter the \texttt{up} state. If it does, but then realizes that any condition is not satisfied for the detection of a left spine, it falls back to \texttt{reset}. Alternatively, it may detect the end of an ``upward'' phase, and make a transition to the \texttt{down} state. Once there, the detection of a left spine, of height $h = 1$ or greater, is guaranteed. The automaton greedily iterates itself until some condition in the \texttt{down} state is no longer satisfied, and then terminates in the \texttt{accept} state. The calling algorithm, when notified of the acceptance, collects the $(a, b)$ range and resets the state of the automaton with a call to \texttt{reset()}. \texttt{reset()} does not bring $i$ back to $0$, but merely resets the state and a few other internal variables.

    The explanation for the right-spine automaton is analogous, except that the automaton looks for a pattern of alternating $0$ and $1$ pairs in $\sla$. Two sibling nodes of a right spine $i$ and $i + 1$ are such that $\spi[i] = \spi[i + 1] = \sal[i - 1] \spi[i - 1]$. \linebreak

    The correctness of \algoref{algo.spine.detection} follows almost directly by the two automata, and the correctness of these can be verified in detail by \figref{fig.automaton.left} and \figref{fig.automaton.right}.

    To analyze the complexity of \algoref{algo.spine.detection}, consider its \texttt{while} loop. Clearly, this runs as long as \texttt{automaton.has\_next()} which, by definition, returns \texttt{true} at most $|T|$ times. The space complexity depends from $|\texttt{spines}|$. This is not constant, but it can be made so by the use of \emph{generator functions} that generate, rather than store in memory, the values of \texttt{spines} on a one-by-one fashion. If \algoref{algo.spine.detection} is implemented on a language without generator functions support, then the space complexity is $\Theta(|\texttt{spines}|)$. \linebreak

    Finally, to prove that \algoref{algo.spine.detection} is able to detect all left \emph{and} right spines in a single run, note that the left and right automata maintain an internal state that is independent from that of the other. It is therefore possible to modify \algoref{algo.spine.detection} in such a way that, on each iteration, it updates both automata. They will terminate in the same number of steps, as long as they are initialized with the same \Xbwm~transform $S$.
\end{proof}
