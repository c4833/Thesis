\section{Overview of Information Theory}
We will dedicate this section to an overview of the core concepts of information theory, so as to set the context for the subsequent section, dedicated to data compression, and the remainder of the thesis.

When possible, we will provide proof of our own statements. For a more in-depth and detailed discussion of the same concepts, you are invited refer to \cite{Cover2006}.
Please note that a basic knowledge of probability theory is assumed throughout this section.

\subsection{Entropy of a Random Variable}

\begin{defi}{Entropy of a Random Variable}{}
    Let $X: \Omega \to S_X$ be a discrete random variable over the support set $S_X$, and let $p(x) = \p(X = x)$ be the PMF (Probability Mass Function) of $X$. We define the \emph{entropy of X} as

    \begin{align}
	H(X) = \sum_{x \in S_X} p(x) \log_2 \frac{1}{p(x)}
	\label{eq.entropy.def}
    \end{align}
\end{defi}

The entropy of a random variable is a scalar, real-valued quantity. The base of the logarithm is usually $2$, in which case one simply writes $\log$ to mean $\log_2$. Otherwise, one writes $\log_b$ to specify a logarithm in a base $b$ other than $2$, and $H_b(X)$ for the entropy of $X$ in base $b$.
We call \emph{bit} the unit of measurement of the entropy when $b = 2$, \emph{nat} when $b = e$ and \emph{dits} when $b = 10$.

As can be seen from \eqref{eq.entropy.def}, the entropy of $X$ is the value of $\log \frac{1}{p(x)}$, averaged over the probabilities of $X$. This corresponds to the definition of an expected value, and therefore

\begin{equation}
    H(X) = \mathbb{E} \left[ \log \frac{1}{p(x)} \right]
\end{equation}

It is often convenient to express the entropy in an equivalent, but simpler, form

\begin{equation}
    H(X) = - \sum_x p(x) \log p(x)
\end{equation}

The quantity $\log \frac{1}{p(x)}$ is termed \emph{self-information} of an event. In other words, let $A \subseteq \Omega$ be an event. Then the self-information, measured in bits, related to the event $A$ is

\begin{equation}
    i(A) = \frac{1}{\log \p (A)} = - \log \p (A)
\end{equation}

where $\p(A)$ denotes the probability of $A$ occurring.

The entropy $H(X)$ of a random variable $X$ denotes the average number of bits we need to describe an outcome of $X$.

\begin{example}{Head vs. Tail Experiment}{}
    Let $X$ represent a head-tossing experiment, so that $S_X = \left\{ 0, 1 \right\}$. Assume that $p(0) = p$, while $p(1) = 1 - p$. Then

    \begin{align}
	H(X)
	&= - \sum_x p(x) \log p(x) \\
	&= - \left( p(0) \log p(0) + p(1) \log p(1) \right) \\
	&= - \left( p \log p + (1 - p) \log (1 - p) \right) \\
	&= -p \log p - (1 - p) \log (1 - p)
	\label{eq.head.tail}
    \end{align}

    Consider \figref{fig.head.tail} for a representation of the entropy of $X$ as the value of $p$ ranges from $0$ to $1$. If we set $p = 1 / 4$ then \eqref{eq.head.tail} becomes $0.81$, meaning that we need, \emph{on average}, $0.81$ bits to encode the value of $X$. The minimum value of this expression is $0$ when $p \in \left\{ 0, 1 \right\}$ (in that case, $X$ is a constant-valued random variable, and we need convey no information to tell the next outcome); the maximum value of it is $1$ when $p = \frac{1}{2}$, that is when either head or tail are equiprobable.
\end{example}

\subimport{InformationTheoryConcepts/}{HeadTailPlot}

\begin{example}{}{}
    Consider a discrete random variable $X$, on a support set $S_X = \left\{ 0, 1, 2, 3, 4 \right\}$, such that

    \begin{equation}
	p(x) =
	\begin{cases}
	    \frac{1}{2} &\quad \text{if $x = 0$} \\
	    \frac{1}{4} &\quad \text{if $x = 1$} \\
	    \frac{1}{8} &\quad \text{if $x = 2$} \\
	    \frac{1}{16} &\quad \text{if $x = 3$} \\
	    \frac{1}{16} &\quad \text{if $x = 4$}
	\end{cases}
    \end{equation}

    The entropy of $X$ is

    \begin{align}
	H(X)
	&= \frac{1}{2} \log 2 + \frac{1}{4} \log 4 + \frac{1}{8} \log 8 + \frac{1}{16} \log 16 + \frac{1}{16} \log 16 \\
	&= \frac{1}{2} + \frac{2}{4} + \frac{3}{8} + \frac{4}{16} + \frac{4}{16} \\
	&= \frac{8 + 8 + 6 + 4 + 4}{16} \\
	&= 1.875 ~ \mathrm{bits}
    \end{align}
\end{example}

\begin{observation}{}{}
    The entropy of a random variable $X$ is maximum when it is uniformly distributed, that is when $X \sim \mathrm{U}\left( S_X \right)$, for a support set $S_X$ of finite size.
\end{observation}

\begin{example}{}{}
    Let $X \sim \mathrm{U}\left( \left\{ 0, 1, \ldots, 7 \right\} \right)$, so that $\forall x, \; p(x) = \frac{1}{8}$. Then

    \begin{align}
	H(X)
	&= \underbrace{\frac{1}{8} \log 8 + \cdots + \frac{1}{8} \log 8 }_{8} \\
	&= 8 \cdot \frac{1}{8} \log 8 \\
	&= 3 ~ \mathrm{bits}
    \end{align}

    In general, the entropy of a uniform discrete random variable $X$ on a support set $S_X$ is $\log |S_X|$ bits.
\end{example}

\subsection{Conditional Entropy}

Just as we have defined the entropy for the case of a single random variable, it is also possible to define it for the case of a vector-valued random variable.
We will give some examples, restricting, for simplicity, to the case of a pair of random variables $(X_1, X_2)$.

\begin{defi}{Join Entropy}{}
    Let $X = (X_1, X_2)$ be a pair of random variables. We define its entropy $H(X)$ as

    \begin{align}
	H(X)
	&= \sum_{x_1 \in S_{X_1}} \sum_{x_2 \in S_{X_2}} p(x_1, x_2) \log \frac{1}{p(x_1, x_2)}
    \end{align}

    where $p(x_1, x_2) = \p\left( X_1 = x_2, X_2 = x_2 \right)$ is the PMF of $X$.
\end{defi}

A very important concept is the \emph{conditional entropy} of a random variable $X$, defined with regard to another random variable $Y$.

\begin{defi}{Conditional Entropy}{}
    Given two random variables $X$ and $Y$, the \emph{conditional entropy of $X$ given $Y$} is

    \begin{align}
	H(X | Y)
	&= \sum_{x \in S_X} p_X(x) H(Y | X = x) \\
	&= \sum_{x \in S_X} p_X(x) \sum_{y \in S_Y} p_Y(y | x) \log p_Y(y | x) \\
    \end{align}

    where $p_X$ and $p_Y$ are the PMFs of $X$ and $Y$, respectively\footnotemark.
    
    \footnotetext{
	We take $p_Y(y|x)$ to be a shorthand notation for $\p\left( Y = y ~\middle|~ X = x \right)$.
    }
\end{defi}

The two notions of joint entropy and conditional entropy are related in an important way, going under the name of \emph{chain rule}.

\begin{theorem}{}{}
    For any two random variables $X$ and $Y$

    \begin{equation}
	H(X, Y) = H(X) + H(Y|X)
    \end{equation}
\end{theorem}

\begin{proof}
    Denote $\p\left( X = x, Y = y \right)$ simply by $p(x, y)$, then

    \begin{align}
	H(X, Y)
	&= - \sum_{x \in S_X} \sum_{y \in S_Y} p(x, y) \log p(x, y) \tag{By definition} \\
	&= - \sum_{x \in S_X} \sum_{y \in S_Y} p(x, y) \log \left( p_X(x) \cdot p_Y(y|x) \right) \\
	&= - \sum_{x \in S_X} \sum_{y \in S_Y} p(x, y) \log p_X(x) - \sum_{x \in S_X} \sum_{y \in S_Y} p(x, y) \log p_Y(y|x) \\
	&= - \sum_{x \in S_X} p_X(x) \log p_X(x) - \sum_{x \in S_X} \sum_{y \in S_Y} p(x, y) \log p_Y(y|x) \\
	&= H(X) + H(Y|X)
    \end{align}
\end{proof}

\begin{cor}{}{}
    For any three random variables $X$, $Y$ and $Z$

    \begin{align}
	H(X, Y|Z) = H(X|Z) + H(Y|X, Z)
    \end{align}
\end{cor}

\subsection{Relative Entropy and Mutual Information}
We will briefly hint to a measure known as \emph{mutual information}, defined between a pair of random variables. Informally, mutual information expresses the amount of information that one random variables conveys about another.

Before actually defining mutual information, we have to step through another quantity.

\begin{defi}{Relative Entropy}{}
    Let $X$ and $Y$ be two random variables with PMFs $p_X$ and $p_Y$. We define the relative entropy of $X$ and $Y$ as

    \begin{align}
	D\left( p_X ~||~ p_Y \right)
	= \sum_{x \in S_X} p_X(x) \log \frac{p_X(x)}{p_Y(x)}
	\label{eq.relative.entropy}
    \end{align}

    Note that $p_Y$ is fed values $x \in S_X$, so that the denominator in \eqref{eq.relative.entropy} may equal $0$. However, due to analytical considerations, it is assumed by convention that $\forall p, p \log \frac{p}{0} = \infty$, so that if $\exists x \in S_X | p_X(x) > 0 \land p_Y(x) = 0$, $D\left( p_X ~||~ p_Y \right) = \infty$.
\end{defi}

\begin{prop}{}{}
    For any two random variables $X$ and $Y$

    \begin{enumerate}
	\item $D\left( p_X ~||~ p_Y \right) \geq 0$
	\item $D\left( p_X ~||~ p_Y \right) = 0$ if and only if $p_X = p_Y$
    \end{enumerate}
\end{prop}

\begin{defi}{Mutual Information}{}
    The \emph{mutual information} $I(X; Y)$ between two random variables $X$ and $Y$ is the relative entropy of the joint distribution $p_{(X, Y)}$ of $X$ and $Y$ and the product distribution $p_X(x) p_Y(y)$

    \begin{align}
	I(X; Y)
	&= \sum_{x \in S_X} \sum_{y \in S_Y} p_{(X, Y)}(x, y) \log \frac{p_{(X, Y)}(x, y)}{p_X(x) p_Y(y)} \\
	&= D \left( p_{(X, Y)}(x, y) ~||~ p_X(x) \cdot p_Y(y) \right)
    \end{align}
\end{defi}

The mutual information of two random variables can be expressed as

\begin{equation}
    I(X; Y) = H(X) - H(X|Y)
\end{equation}

In fact

\begin{align}
    I(X; Y)
    &= \sum_{x \in S_X} \sum_{x \in S_Y} p_{(X, Y)}(x, y) \log \frac{p_{(X, Y)}(x, y)}{p_X(x) p_Y(y)} \\
    &= \sum_{x, y} p_{(X, Y)}(x, y) \log \frac{p_X(x | y)}{p_X(x)} \\
    &= \sum_{x, y} p_{(X, Y)}(x, y) \log p_X(x | y) - \sum_{x, y} p_{(X, Y)} \log p_X(x) \\
    &= - \left( \sum_{x, y} p_{(X, Y)}(x, y) \log p_X(x | y) \right) - \sum_{x, y} p_{(X, Y)} \log p_X(x) \\
    &= - H(X|Y) + H(X)
\end{align}

In this form, $I(X; Y)$ tells the amount of information of $X$ we spare by observing $Y$. Note that, due to symmetry, it also holds that $I(X; Y) = H(Y) - H(Y|X)$. 

Finally, since $I(X; X) = H(X) - H(X|X) = H(X)$, we get that the mutual information of a variable with itself is the entropy. For this reason, the entropy of a random variable is also called \emph{self-information}.
