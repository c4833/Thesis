\subsection{Codes}
The algorithms we will be considering work by mapping the symbols of an input alphabet $A$ to so-called \emph{codewords}, strings built on top of an alphabet $B$, possibly equal to $A$ itself. These algorithms, when scanning its input, associate a codeword to a single symbol $a \in A$, or to more symbols of $A$ packed together. We call an association of codewords to strings of symbols $s \in A^*$ a \emph{code}.

One remark concerns how we pack more codewords together to form the compressed output. We may use a separator character between each codeword, but this would be far from being an efficient method. In a data compression scenario, we strive to squeeze our output as much as possible.
Instead, we will simply pack our codewords one next to the other, without any padding in the middle or any header information before the output. What is required is to be able to recognize a codeword on the basis of its symbols alone.

\subimport{Codes/}{CodeTypes}
And now, for the actual codes. Consider \tabref{tab.codes}. In that table, we included an alphabet of four symbols, along with three possible codes. We'll introduce some common code classifications on the basis of these examples. They are all variable-length codes, meaning that, if the probability of an individual symbol was higher than that of another, we may be able to achieve compression\footnotemark.

\footnotetext{
    We assume that the least advantageous way of compressing the $\left\{ a_0, a_1, a_2, a_3 \right\}$ alphabet is by assuming an uniform distribution, and using $\log_2 4 = 2$ bits per each symbol.
}

Let's start with Code 1. Code 1 is noticeably flawed: the same codeword, \texttt{0}, is mapped to two different symbols, $a_0$ and $a_2$. If we met a $0$ in the output stream, we wouldn't be able to tell whether it came from $a_0$ or $a_2$. We call codes like Code 1 \emph{ambiguous codes}.

We then have a look at Code 2. Although Code 2 is not ambiguous, it is not free of defects. Imagine we had to decode the string $\omega = \texttt{0100}$. One way to decode this string would be to split it as \texttt{0 1 00}, and interpret its inverse image as $a_0 a_1 a_2$. However, if we split it as \texttt{0 10 0}, we would be getting the equally plausible inverse image $a_0 a_3 a_0$.
This, clearly, is an undesirable situation to us. What we would like to have is a code that, unlike Code 2, admits one and only one possible interpretation for each combination of its codewords. Such a code is termed \emph{uniquely decodable code}. Code 2 is not uniquely decodable.

Let us finally take a look at Code 3. Clearly, this code is unambiguous. Further, it exhibits an important property: no codeword is the prefix of any other codeword. Codes with this property go under the name of \emph{prefix codes}. Prefix codes are also \emph{instantaneous codes}, that is codes whose codewords can be interpreted as soon as they are read, with no need to read the whole string they are embedded in.
What we are most pressed to remark though is that, Code 3, like any other prefix code, is uniquely decodable. This is true because, no matter how we mix up the codewords together, the union of two codewords $\omega_1, \omega_2$ never produces a third distinct codeword $\omega_3$. (If this did happen, then $\omega_1$ would be a prefix for $\omega_3$.)
Being a uniquely decodable code, Code 3 is a viable code for the $\left\{ a_0, a_1, a_2, a_3 \right\}$ alphabet of \tabref{tab.codes}.

On more abstract terms, we may wonder: if every prefix code is a uniquely decodable code, what can we say about uniquely decodable codes in general? Are there non-prefix, uniquely decodable codes that are shorter than some prefix code? Fortunately for us, this is not the case, as the following two statements demonstrate.

\begin{theorem}{Kraft-McMillan inequality}{kraft.mcmillan}
    Let $C$ be a uniquely decodable code with $N$ codewords of length $l_1, l_2, \ldots, l_N$. Then

    \begin{equation}
	\sum_{i = 1}^{N} 2^{l_i} \leq 1
	\label{eq.kraft.mcmillan}
    \end{equation}
\end{theorem}

The relation in \eqref{eq.kraft.mcmillan} is known as \emph{Kraft-McMillan inequality}. Next we have

\begin{theorem}{}{}
    For every set of integers $l_1, l_2, \ldots, l_N$ satisfying the inequality

    \begin{equation}
	\sum_{i = 1}^{N} 2^{- l_i} \leq 1
    \end{equation}

    it is possible to find a prefix code whose codewords have lengths $l_1, l_2, \ldots, l_N$.
\end{theorem}

These two statements allow us to deduce the following fact: if we are given a uniquely decodable code $C$, then $C$ satisfies the Kraft-McMillan inequality; and if it satisfies the Kraft-McMillan inequality, then a prefix code exists whose codewords have the same length as the codewords of $C$. Thus, we lose nothing when focusing on prefix codes.
