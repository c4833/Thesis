\paragraph{LZ77}
\label{sec.dictionary}
LZ77 is a classical lossless textual compression algorithm, introduced by Abraham Lempel and Jacob Ziv in 1977 \cite{Ziv1977}. Together with LZ78 \cite{Ziv1978}, published in 1978, they form the basis for a series of modifications, such as LZW, LZMA, LZSS, introduced by other researchers throughout the years.

% LZ77 is a dictionary coder. Dictionary coders are compression algorithms that make use of an internal dictionary for translating a series of symbols into keywords stored within the dictionary.
LZ77 is a dictionary coder. Dictionary coders are compression algorithms that translate strings of symbols from an input string into ``keywords'' stored within the dictionary as a mapping for these sequences.
If the content of the dictionary is not allowed to change during the execution, then we are dealing with a static dictionary coder, otherwise, just as with LZ77 and LZ78, we are dealing with a dynamic one.

To describe the functioning of LZ77, we start from the underlying idea. Take a string of text $s = s_1 s_2 \ldots s_n$, built from a series of symbols from an alphabet $\Sigma$. Then we can split $s$ into two halves $s'$ and $s''$, such that $s = s' s''$. $s'$ is considered to be the part of the input stream that is already compressed (also referred as \emph{sliding window} in the literature), while $s''$ the part of the input that is to be yet compressed.
In compressing $s''$, we take a look back at $s'$, to see whether some string of symbols $k \in \Sigma^*$ is occurring there that also occurs in $s''$. If such a $k$ exists, then we take the longest, and output its location in $s'$, plus its length in characters, to the output stream. In addition, the character $c \in \Sigma$ occurring right after $k$ in $s''$ is also output in the output stream\footnotemark.

\footnotetext{
    This is true in the pure LZ77 version. Other variations, such as LZSS, allow this character $c$ to be omitted when it is possible to save some space.
}

This approach is effective the longer $k$ is in characters, since its compressed representation (that is, the triple of values indicating the position of $k$ in $s'$, its length and the character $c$) is constant in size. However, other factors also determine the effectiveness of LZ77. To understand them, we have to take a look at the fuller picture of LZ77.

The crucial bit of information that we have omitted from this informal explanation is that LZ77 has limited buffer, that is a limited memory capacity, and as a consequence also a limited view into both $s'$ and $s''$.
Let $B$ be the number of symbols that LZ77 can hold in its buffer, and $F$ the number of symbols that it is allowed to read from $s''$. The number of symbols readable from $s'$ will consequently be $B - F$. Therefore, the performance of LZ77 depends not only on the concrete input string it is run with, but also on the parameters $B$ and $F$ it is configured with. The larger these values, the greater the length of a possible $k$ will be allowed to be (although, then, running times will also tend to increase).

This tuning aspect is important, and will be considered again in \secref{sec.log.compression}, when we study LZ77 under the problem of logarithmic compression.

Another relevant aspect we want to mention is the size, in bits, of the triple output by the algorithm. The size of the triple is a function of the parameters of LZ77, and therefore constant with respect to the input string $s$. It is expressed as

\begin{equation}
    d = m + \left \lceil \log \left( B - F \right) \right \rceil + \left \lceil \log F \right \rceil
    \label{eq.lz77.d}
\end{equation}

where $m > 0$ is the number of bits needed to represent a character $c \in \Sigma$ ($m = \lceil \log |\Sigma| \rceil$ if $\Sigma$ is given a uniform memorization scheme), $\lceil \log \left( B - F \right) \rceil$ is the number of bits needed to express the position of $k$ in $s'$ (as a relative offset starting at $s''$ and going backwards for up to $B - F$ symbols) and $\lceil \log F \rceil$ the number of bits needed to encode the length of $k$.

If LZ77 parses its input string $s$ into $N$ distinct triples, the size of its output will therefore be $dN$.
