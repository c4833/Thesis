\paragraph{Huffman Coding}
The Huffman algorithm is an optimal prefix code developed by David A. Huffman in 1952 \cite{Huffman1952}, that assumes a probability distribution over a source alphabet $\Sigma$.
For our own purposes, we will assume the algorithm to work on the binary alphabet $\left\{ 0, 1 \right\}$ as a codomain, so that, $\forall s \in \Sigma^*, C(s) \in \left\{ 0, 1 \right\}^*$, where $C$ denotes the application of the Huffman encoding.

The central idea of the method is to construct a mapping for each symbol $\sigma \in \Sigma$ to a sequence of bit strings in $\left\{ 0, 1 \right\}^*$, so that, if the probability of a symbol $\sigma_1 \in \Sigma$ is higher than that of another symbol $\sigma_2 \in \Sigma$, then the codeword $C(\sigma_1)$ for $\sigma_1$ is shorter than the one for $\sigma_2$.
In other terms

\begin{equation}
    \p(\sigma_1) > \p(\sigma_2) \implies |C(\sigma_1)| < |C(\sigma_2)|
    \label{eq.huff.less}
\end{equation}

To simplify understanding, we provide a small example for the construction of a Huffman code for the alphabet $\Sigma = \left\{ \mathrm{a, b, c, d, e} \right\}$.
There are various ways to depict the construction of a Huffman code. The one we utilise relies on a binary tree.

Consider \tabref{tab.huff}, where the symbols from $\Sigma$ have been sorted according to their own probability $\p(\sigma)$.
We build the binary tree starting from the symbols with the lowest probability. This tree satisfies the following properties


\begin{table}
    \centering
    \begin{tabular}{|l|l|}
	\hline
	Symbol & Probability \\
	\hline
	c & $0.5$ \\
	e & $0.25$ \\
	a & $0.1$ \\
	d & $0.1$ \\
	b & $0.05$ \\
	\hline
    \end{tabular}
    \caption{Example probability distribution of a source alphabet $\Sigma$ for the Huffman algorithm. The entropy of this distribution is $1.88$ bits per symbol.}
    \label{tab.huff}
\end{table}

\begin{itemize}
    \item Every edge connecting two nodes is labeled with either $0$ or $1$
    \item Each symbol $\sigma \in \Sigma$ is represented by one and only one leaf node
    \item Internal nodes of the tree correspond to no symbols from $\Sigma$
\end{itemize}

The tree building procedure, whose complete formal steps we omit for brevity, applied to the probability distribution of \tabref{tab.huff}, results in the tree of \figref{fig.huff}.
The codewords thus obtained for the $\Sigma$ alphabet are shown in \tabref{tab.huff.codewords}.

\subimport{HuffmanCoding/}{Tree}

\begin{table}
    \centering
    \begin{tabular}{|l|l|}
	\hline
	Symbol & Codeword \\
	\hline
	c & $0$ \\
	e & $10$ \\
	a & $110$ \\
	d & $1111$ \\
	b & $1110$ \\
	\hline
    \end{tabular}
    \caption{Codewords for the alphabet $\Sigma$ of \tabref{tab.huff}.}
    \label{tab.huff.codewords}
\end{table}

With a mapping between symbols of $\Sigma$ and binary codewords, we can translate a string $s \in \Sigma$ into one from $\left\{ 0, 1 \right\}^*$.
Assume, for example, that $s = \mathrm{cedcea}$. Then

\begin{equation}
    C(s) = 0 \cdot 10 \cdot 1111 \cdot 0 \cdot 10 \cdot 110
\end{equation}

where $\cdot$ is just a visual aid with no implications for the representation of $C(s)$.

Recall that the entropy of the probability distribution of \tabref{tab.huff} is $1.88$ bits per symbol, meaning that, for a string of $6$ characters, we would need, on average, $6 \times 1.88 = 11.28$ bits for transmitting it, provided we had an optimal encoding method.

While, in general, Huffman coding is not perfectly optimal with respect to the theoretical entropy of a source, it performs pretty close to it.

For instance, if we were to represent $\Sigma$ with a uniform encoding method, using $\lceil \log_2 5 \rceil = 3$ bits per symbol, we would need $6 \times 3 = 18$ bits to transmit $s$.
With Huffman encoding, instead, we only need $13$, since $|C(s)| = 13$.

In general, the higher the number of symbols in $s$ with high probability, the higher also the space saving of its Huffman-encoded version $C(s)$.
