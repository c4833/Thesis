\documentclass[class=MastersDoctoralThesis, crop=false]{standalone}
\usepackage[subpreambles=true]{standalone}

\begin{document}
    \chapter{Spine Detection}
    \label{chap.spine.detection}
    Before dealing with the problem of spine compression, we have to find ways to detect spines within trees of arbitrary shapes. One simple and immediate way is for a spine detection algorithm to work on the hidden representation of the tree $T$, which we assume to support basic navigational operations. While easy, this solution has the potential drawback of working with a representation that does not fully exploit the spatial locality principle, i.e. the behavior of accessing memory cells spatially close to each other. In fact, a pointer-based representation of a tree $T$ may dislocate the data of $T$ in disparate central memory locations, thus making the whole cache hierarchy less performant.

    Instead, we may take advantage of the fact that $\xbw[T]$ is a vector stored in a contiguous memory space. Not only does $\xbw[T]$ use less memory space, but it is also more rapid to iterate on than the potentially pointer-based representation of $T$.

    \section{\Xbwm~Transform}
    In what follows, we define a concept derived from the XBW transform, the \emph{\Xbwm~transform}, which encodes peculiar patterns of left and right spines which we exploit for their identification. We first give an intuitive overview of the detection scheme and then proceed to formalize it mathematically. We conclude the section with a practical algorithm to detect left and right spines in optimal $\Theta(t)$ time and $\Theta(1)$ space.

    \begin{defi}{\Xbwm~transform}{}
	An \Xbwm~transform of a tree $T$, denoted by $\xbwm[T]$, is the XBW transform of $T$ deprived of the sorting step with respect to $S_{\pi}$.
    \end{defi}

    \begin{observation}{}{}
	Computing an \Xbwm~transform demands no computational overhead, if we expect to compute the full XBW transform at a later time.
    \end{observation}

    In \figref{fig.xbwm.example}, we give an example of \Xbwm~transform for the tree of \figref{fig.xbw.example}.

    \subimport{SpineDetection/}{XBWMinusTransform.tex}

    \begin{observation}{}{}
	Since no sorting is performed, the order of the entries in an \Xbwm~transform reflects that of a pre-order visit.
    \end{observation}

    \section{Informal Detection Description}

    The central idea behind the identification of left and right spines is inspecting the $\sla$ and $S_{\pi}$ subvectors of the \Xbwm~transform. Consider, by way of example, the left spine of \figref{fig.spine.example.left}, along with its \Xbwm~transform, that we show in \figref{fig.spine.xbwm.left}.

    \subimport{SpineDetection/}{LeftSpine}

    Clearly, some visible patterns emerge from \figref{fig.spine.xbwm.left.xbwm}. Let $h = 4$, the height of the spine. Then $\sla$ exposes a series of $h$ contiguous $0$s, followed by another consecutive $h$ $1$s, while $S_{\pi}$ is characterized by a kind of ``symmetry'' for all the nodes except the root.

    Albeit dissimilar, even right spines display their own repetitive pattern. See \figref{fig.spine.xbwm.right} for a reference. In this case, an alternating series of $0$s and $1$s takes place of the contiguous series of $0$s and $1s$ in the left-spine case. This time, $S_{\pi}$ is characterized by a sort of ``increasing monotonicity'', rather than a symmetry.

    \subimport{SpineDetection/}{RightSpine}

    Having introduced informally the detection of left and right spines by means of their \Xbwm~transform, we make our discourse more precise by stating the following general propositions.

    \subimport{SpineDetection/}{Properties}

    Now that all major properties of \Xbwm~transforms on spine trees have been proved, we introduce a practical algorithm for the identification of spines.

    \subimport{SpineDetection/}{DetectionAlgorithm}
\end{document}
