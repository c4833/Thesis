\section{Basic Concepts and Notation}

\subsection{Mathematical Notation}
\paragraph{Integer Interval}
We write $\irange{i}{j}$ for the set of all integers between $i$ and $j$, that is $\irange{i}{j} = \left\{ x \in \Z ~\middle|~ i \leq x \leq j \right\}$. In those rare cases---if any---where we want to designate the real-valued interval, we will write $[i, j]_{\R}$, and more generally, $\forall Q \subseteq \R$, we let $[i, j]_{Q} = \left\{ x \in Q ~\middle|~ i \leq x \leq j \right\}$.

\paragraph{Congruence Modulo $n$}
We will denote the congruence modulo $n$ of two numbers $a$ and $b$ as $a \equiv_n b$.
This is a departure from the more common notation $a = b ~ \mod n$ of some authors, but we prefer it for its succinctness.

\paragraph{Internal and Leaf Node Labels}
As in \cite{Ferragina2009}, we adopt the convention of representing the internal nodes of a labeled tree with values drawn from an alphabet $\Sigma_N$, and leaf nodes with values drawn from an alphabet $\Sigma_L$, so that $\Sigma = \Sigma_N \cup \Sigma_L$. This convention only affects the presentational aspect. Internally, an additional bit for each node may be used to distinguish internal nodes from leaf ones.

\paragraph{Vector Indexing}
Let $V$ be a vector. We write $V[i]$ for the $i$th element of $V$, and $V[i, j]$ for all elements of $V$ such that their index $k \in \irange{i}{j}$.

\subsection{XBW Transform}
In this section, we summarize some of the key aspects of the XBW transform. If interested to get the fuller picture, please refer to \cite{Ferragina2009}.

Take an ordered and labeled tree $T$, of arbitrary shape and depth.
In order to define the XBW transform $\xbw[T]$ of $T$, we need the following elements.
Let $u$ be any node of $T$, then we consider

\begin{itemize}
    \item $\mathrm{last}[u]$, a binary value equal to $1$ if $u$ is the rightmost (i.e. last) child of its parent
    \item $\alpha[u]$, the label, or value, associated to $u$
    \item $\pi[u]$, the string obtained by concatenating all the labels from $u$'s parent up to the root. If $u$ is the root node, $\pi[u] = \varepsilon$, the empty string
\end{itemize}

The XBW transform of $T$ consists then of the multi-set $S$ of $t$ triplets $\langle \last[u], \alpha[u], \pi[u] \rangle$, one for each node of $T$. The transform is constructed according to the following procedure

\begin{enumerate}
    \item Initially, set $S = \emptyset$
    \item Perform a pre-order visit of $T$
    \item For each visited node $u$, store $\langle \last[u], \alpha[u], \pi[u] \rangle$ into $S$
    \item Stably sort $S$ with respect to the lexicographic order of $\pi[u]$
\end{enumerate}

To simplify understanding, \figref{fig.xbw.example} depicts a labeled tree $T$, along with its XBW transform $\xbw[T]$.

\subimport{BasicConcepts/}{ExampleXBWTransform}

Given $\xbw[T]$, we sometimes identify each node $u \in T$ with an integer $i$, that is $u = S[i]$. In such a case, we denote $\last[u]$, $\alpha[u]$ and $\pi[u]$ with $\sla[i]$, $\sal[i]$ and $S_{\pi}[i]$ respectively, or even $\mathrm{last}_i$, $\alpha_i$ and $\pi_i$ for the sake of brevity.

Note that the vector $\Xbw$, consisting only of $\sla$ and $\sal$, is sufficient for recovering the whole structure of $T$. In fact, $\spi$ does not need to be stored in a compressed representation of $\xbw[T]$, since it can be reconstructed during the decompression process.

\cite{Ferragina2009} discusses different results concerning the XBW transform on trees.
We report a few of the more important ones, omitting their proof.

\begin{theorem}{}{}
    Let $T$ be a labeled tree with $t$ nodes and labels drawn from an alphabet $\Sigma$. Then the transform $\xbw[T]$ can be computed in $O\left( t \right)$ time and $O\left( t \log t \right)$ bits of working space.
\end{theorem}

\begin{theorem}{}{}
    A labeled tree $T$ of $t$ nodes can be reconstructed from its XBW transform $\xbw[T]$ in optimal $O\left( t \right)$ time and $O\left( t \log t \right)$ bits of working space.
\end{theorem}

The XBW transform is interesting for the task of tree compression because of the locality principle.
The locality principle for a string $s \in \Sigma^*$ states that the surrounding elements of a value in $s$ closely depend from the predecessor and successor values.
Due to the sorting step of $\xbw[T]$, $\sal$ satisfies the locality principle and can be compressed efficiently by BWT-based compressors \cite{Burrows1994, Ferragina2005b}.
