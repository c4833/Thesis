\paragraph{Run-Length Coding}
\label{sec.par.rle}
We finally take a look at a candidate algorithm for the problem of logarithmic compression, given by the combination of the Burrows-Wheeler Transform \cite{Burrows1994} and run-length coding, that is fast and has a wide restriction domain $\dom_{\log} C$. 
To the best of our knowledge, this is the best solution we could come up with.

Run-length coding is a compression scheme acting on runs of repeated characters. When an input string is formed by a series of identical symbols, they are substituted by a shorter description, indicating the character that is being repeated and the number of times it appears in the source text. For example, if our input string $x = \texttt{AAAAABBB}$, then $C(x) = \texttt{5A3B}$. 
With this algorithm, we find it easy to characterize, although in part, the restriction domain $\dom_{\log}~C$ of $C$.
For example, for sufficiently large values of $n$

\begin{align}
    0^n & \in \dom_{\log, 1} ~ C \\
    0^n 1^n & \in \dom_{\log, 2} ~ C \\
    0^n 1 0^n & \in \dom_{\log, 2} ~ C
    \label{eq.rlc}
\end{align}

Run-length coding is a simple compression algorithm, but its scope of application may seem limited at first. However, consider the following. On one hand, the set of logarithmically-compressible strings is known to be ``limited'' and somehow ``simple'', from the point of view of information theory.
On the other hand, run-length coding can find application beyond the set of inputs described in \eqref{eq.rlc}.
For this, it suffices to consider the BWT (Burrows-Wheeler Transform) \cite{Burrows1994}.

The BWT is not a compression algorithm per se, but a transformation between strings from the same alphabet. Given an input string $x$, the BWT produces an output string $x'$ containing the same symbols as $x$, but rearranged in such a way as to form runs of repeated characters. This process improves the compression ratio of algorithms that apply the MTF (Move to Front) transform, or directly apply run-length coding, as in our case. Although hard to analyze mathematically, the combination of run-length coding and BWT makes for a promising practical solution to our problem of logarithmic compression.

As an example, take the string $x = \texttt{ABABABAB}\ldots\texttt{AB}$.
If $\forall c \in A, C(c) = c$, then $C(x) = x$, meaning that we obtain no compression at all.
However, consider the application of the BWT to $x$ to obtain a string $x'$. In this case, we have that

\begin{equation}
    x' = \underbrace{\texttt{AAA\ldots A}}_{\frac{|x|}{2}} \cdot \underbrace{\texttt{BBB\ldots B}}_{\frac{|x|}{2}}
\end{equation}

We can now apply the run-length coding to $x'$; if $x'$ is long enough, we get a logarithmic compression; otherwise, an equally good compression factor.

Run-length coding would also achieve a better compression ratio, with lower computational resources consumption, with the type of LCFs (Logarithmically Compressible Files) described in the LZ77 paragraph, since these files are constituted by a run of repeated characters.

\subparagraph{Experimental Outcomes}
To validate the choice of the BWT plus RLE approach (BWT--RLE), some experiments were run.
As a first attempt, consider an input string sequence of the form $x = \texttt{AB}^{2^i}$, with $i \in \left\{ 1, \ldots, 16 \right\}$, like the one just mentioned (higher values of $i$ weren't considered due to computational overhead and lack of performance optimization). The experiment of compressing this first type of sequence is illustrated in \figref{fig.bwtrle.1}.
With a tendency such as the one shown in the picture, it also emerges from an experimental basis that for sufficiently large values of $i$, $\texttt{AB}^i \in \dom_{\log, 2} C$, and possibly, even that $\texttt{AB}^i \in \dom_{\log, 1} C$, where $C$ denotes the BWT--RLE combination.

\subimport{RunLength/}{BWTRLEResults1}
\subimport{RunLength/}{BWTRLEResults2}

Similar results are obtained if we extend the iterated character pair \texttt{AB} to a longer sequence, like \texttt{ABCDE}. In fact, thanks to the BWT, we have that

\begin{equation}
    \mathrm{BWT}(\texttt{ABCDE\cdot ABCDE\cdot \ldots \cdot ABCDE}) = \underbrace{\texttt{AAA\ldots A}}_{\frac{n}{5}} \cdot \underbrace{\texttt{BBB\ldots B}}_{\frac{n}{5}} \cdots \underbrace{\texttt{EEE\ldots E}}_{\frac{n}{5}}
\end{equation}

\figref{fig.bwtrle.2} shows how the BWT--RLE algorithm performed with $\texttt{ABCDE}^{2^i}$.
