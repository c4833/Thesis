\paragraph{LZ77}
\label{sec.lz77}
Recall, from section \ref{sec.dictionary}, when we talked about Lempel-Ziv compression methods. In the current paragraph, we will investigate the performance of LZ77 \cite{Ziv1977} with reference to logarithmic compression.
As we are going to see, LZ77 acts as a solution of intermediate effectiveness, since it is able to perform well only for inputs of a limited size.

We will denote by $C$ the LZ77 compression algorithm, and by $x$ an arbitrary input string of $n$ symbols; if the alphabet of $x$ is $A$, then the binary size of $x$, $l(x)$, is given by $m n$, where $m = \lceil \log |A| \rceil$.

As we have seen, LZ77 has a limited view into the compressible sequence, being limited to scan no more than $F$ symbols of its input string. Therefore, the best it is able to perform is scan its whole input, split it up into $\frac{n}{F}$ ``chunks'' and compress those into an equal number of blocks of size $d$ each. Clearly, as the length $n$ of the sequences we expect to compress increases, we have to adapt the algorithm parameters accordingly.

To achieve logarithmic compression with LZ77 means to find a constant $c \geq 1$ and a subset $D \subseteq \dom~C$ such that, $\forall x \in D$, $l(C(x)) \leq c \log \left( m n \right)$ (see \eqref{eq.logarithmic.problem}). LZ77 works by scanning the input string $x$, dividing it into $N$ chunks $x_i$ of possibly distinct length, and compressing them into fixed-sized blocks of length $d$ each (recall the definition of $d$ from \eqref{eq.lz77.d})

The binary length, expressed in bits, $l(C(x))$ of the algorithm output is therefore given by $d N$. Since we want it to be no longer than $c \log (n m)$ we have that

\begin{equation}
    d N \leq c \log (n m)
\end{equation}

or

\begin{equation}
    N \leq \frac{c}{d} \log \left( nm \right)
    \label{eq.N.max}
\end{equation}

\eqref{eq.N.max} tells us how many chunks $x$ can be split into at most. Since $mn$ grows more quickly than $\log (mn)$ for greater values of $n$, the greater $n$, i.e. the longer $x$, the more difficult it will be to compress $x$ down to a logarithmic factor. To get a sense of how this is true, take a look at \tabref{tab.logarithmic.table}.

\subimport{LZ/}{LogarithmicTable}

In \tabref{tab.logarithmic.table}, we set $m = 8$ bits, $B = 1024$, $F = 512$; each row of the table reports an input length, in bytes, as a power of two, the maximum number of chunks $N$ that an input of that length can be split into by the LZ77 algorithms, the average number of symbols per chunk, and the output size, in percentage, of a compressed version of the string if the LZ77 algorithm was able to parse its input in exactly $N$ chunks.
As we can see, although the size of these inputs increases exponentially, the maximum number of allowed chunks for these inputs is only incremented by a constant of $10$ per row. To be able to consistently achieve logarithmic compression as the input size increases is a feat getting harder and harder as we progress toward greater input sizes.

To experiment with this situation, we set up a compression experiment for LZ77. We considered files of exponentially growing size, from $1$ KiB = $2^{10}$ bytes up to $1$ MiB = $2^{20}$ bytes. These files---denoted as LCFs (Logarithmically Compressible Files)---are formed by a concatenation of a limited number of chunks, each composed by the repetition of a single character. In the experiment we conducted, we fixed the number of distinct chunks to $10$ and their length equal to $\frac{B - F}{5}$, so that about $5$ chunks can fit into the window buffer of LZ77. An example LCF of $1$KiB is shown in \figref{source.lcf.10}.

\begin{figure}[h]
    \centering
    \verbinclude{Sections/SpineCompression/LogarithmicCompression/Solutions/LZ/lcf-10.lz77}
    \caption{An example of a LCF (Logarithmically Compressible File) of $1$KiB. The name for these files is a convenience, since LCFs aren't necessarily logarithmically compressible, and if they are, they are with respect to one algorithm, without necessarily being with respect to another.}
    \label{source.lcf.10}
\end{figure}

The outcome of our experiment is visualized in \tabref{tab.lcf.experiment}, where the first column denotes the number of bytes for a given LCF and the second the number of blocks that LZ77 divided its input into. Knowing that each of these blocks is encoded in $d$ bits, LZ77 would be able to compress these files with just $dN$ bits. The halfway horizontal line splitting \tabref{tab.lcf.experiment} in two halves demarcates the extent by which LZ77 is able to achieve logarithmic compression with $c = 10$, i.e. about $32$KiB. This conclusion is derived from \tabref{tab.logarithmic.table}, where the maximum number of allowed chunks $N$ for an input of $2^{15}$ bytes is $180$.
If our requirements aren't excessive and our expected input is not overly complex, we can put LZ77 to good use and expect to receive a logarithmic compression factor.

\subimport{LZ/}{LCFExperimentOutcome}
