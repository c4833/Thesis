\section{Problem Statement}
\label{sec.log.compression.ps}
Consider a compression algorithm $C$, and denote by $\dom~C$ the domain upon which compression by $C$ is possible. Suppose, for the sake of generality, that $\dom~C = A^*$, where $A$ is an alphabet set of arbitrary size. We state the problem of logarithmic compression by $C$ as finding a constant $c \geq 1$ and a subset $D \subseteq \dom~C$ such that, $\forall x \in D$

\begin{equation}
    l(C(x)) \leq c \log \left( l(x) \right)
    \label{eq.logarithmic.problem}
\end{equation}

Here, $C(x)$ denotes the application of $C$ to $x$ to yield a compressed version of $x$, while $l(x)$ denotes the number of bits needed to represent $x$ with some coding convention. For example, if $|x| = t$ and every $x_i \in A$ is encoded uniformly with $m = \left \lceil \log |A| \right \rceil$ bits, then $l(x) = |x| m = tm$.

One objection that could be moved against this definition is that we are being too lax by allowing the choice of a constant $c$. In fact if we, say, were to pick $c = 10^6$, we would consent to the expansion, rather than compression, of an input string $x$.

The rationale for considering logarithmic compression minus a constant factor, however, is that the $\log$ function is a slowly-increasing one, and for inputs $x$ of very large size, logarithmic compression would no longer be possible. Imagine to have a string $x$ of $10^9$ bytes: if we had no regard for constant factors (corresponding to implicitly set $c = 1$), and worked with logarithms in base $2$, then we would force ourselves to compress $x$ with no more than $29$ bytes ($\log_2 10^9 \approx 29.9$).
On the other hand, by even setting $c$ to an as low a value as $2$ or $3$, we expand the set of strings $x$ that can be logarithmically compressed by $C$, without sacrificing much in terms of space performance.

The choice of $c$ is up to the algorithm designer, and is of course fixed before picking any $D \subseteq \dom ~ C$ and $x \in D$.

We can generalize the problem beyond the logarithm function, by considering an arbitrary function $f: \R^+ \to \R^+$.
We denote by $\dom_{f, c}~C$ and call it the \textit{restriction domain of $C$ by $f$} the set of all inputs $x$ which $C$ compresses up to $c f(l(x))$ bits. Formally, given a $c \geq 1$

\begin{equation}
    \dom_{f, c}~C = \left\{ x \in \dom~C ~\middle|~ l(C(x)) \leq c f(l(x)) \right\}
    \label{eq.res.domain}
\end{equation}

When we don't want to be overly precise, or can deduce the value of $c$ from the context, we'll simply write $\dom_f ~ C$ instead of $\dom_{f, c} ~ C$.
